USE blog_db;

INSERT INTO users (email, password, datetime_created) VALUES ("johnsmith@gmail.com", "passwordA", "2021-1-1 01:00:00"), ("juandelacruz@gmail.com", "passwordB", "2021-1-1 02:00:00"), ("janesmith@gmail.com", "passwordC", "2021-1-1 03:00:00"), ("mariadelacruz@gmail.com", "passwordD", "2021-1-1 04:00:00"), ("johndoe@gmail.com", "passwordE", "2021-1-1 05:00:00");
-- Accidentally named my user_id to author_id whoops~
INSERT INTO posts (author_id, title, content, datetime_created) VALUES (1, "First Code", "Hello World!", "2021-1-2 01:00:00"), (1, "Second Code", "Hello Earth!", "2021-1-2 02:00:00"), (2, "Third Code", "Welcome to Mars!", "2021-1-2 03:00:00"), (4, "First Code", "Bye bye solar system!", "2021-1-2 04:00:00");

SELECT * FROM posts WHERE author_id = 1;
SELECT email, datetime_created FROM users;
UPDATE posts SET content = "Hello to the people of the Earth!" WHERE id = 2;
DELETE from users WHERE email = "johndoe@gmail.com";